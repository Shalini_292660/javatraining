
public class Datatypes {
	public static void main(String[] args)
	{
		int numberOfStudents=32;
		System.out.println(numberOfStudents);
		
		long kiloBytesUsed=300_123456_789L;
		System.out.println(kiloBytesUsed);
		
		float temperature=31.45F;
		System.out.println(temperature);
		
		boolean isEligible=true;
		System.out.println(isEligible);
		
	}
}
