
public class LoopsDemo {
 public static void main(String[] args)
 {
	 // While loop
	 
	 int  i=1;
	 while(i<=10)
	 {
		 System.out.println(i);
		 i++;
	 }
	 System.out.println("-----------------");

	 // do while loop
	 
	 int j=20;
	 do
	 {
		 System.out.println(j);
		 j++;
	 }
	 while(j<=15);
		 
 
 //for loop
	 System.out.println("-----------------");
 for(int k=1;k<=20;k++)
 {
	 System.out.println(k);
	 
 }
 }}
