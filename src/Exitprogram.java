import java.util.Scanner;

public class Exitprogram {
	public static void main(String[] args) {
		/*
		 * Scanner sc = new Scanner(System.in); String Input = ""; while
		 * (!Input.equals("exit")) { System.out.print("Input"); Input = sc.nextLine();
		 * System.out.print(Input); } System.out.println("---Exiting---");
		 */

		Scanner sc = new Scanner(System.in);
		String Input = "";
		while (true) {
			System.out.print("Input");
			Input = sc.nextLine();
			if (Input.equals("exit")) {
				break;
			}

			if (Input.equals("pass")) {
				continue;
			}
			System.out.print(Input);
		}
		System.out.println("---Exiting---");
	}
}