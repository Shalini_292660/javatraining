// Variable by name of role
//If the role is developer -->dev@1234
//If the role is admin -->Admin@456
//If the role is Tester-->Tester@1234
// If role does'nt match the above list --> Guest@123
public class SwitchCase {

	public static void main(String[] args)
	{
		String role="Developer";
		
		switch(role)
		{
		case "Developer":
			System.out.println("Dev@1234");
			
		case "Tester":
			System.out.println("Tester@1234");	
		
		case "Admin":
			System.out.println("Admin@1234");
			
		default:
			System.out.println("Guest@1234");
			break;
		}
	}
}
