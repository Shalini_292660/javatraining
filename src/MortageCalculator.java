import java.util.Scanner;

public class MortageCalculator {
	public static void main(String[] args) {
		/*
		 * System.out.print("----------Welcome to AU Small Finance Bank-------------");
		 * 
		 * Scanner sc=new Scanner(System.in);
		 * 
		 * int percentage=100; int monthsInAYear=12;
		 * 
		 * System.out.print("Principal"); int principal=sc.nextInt();
		 * 
		 * System.out.print("Annual Interest Rate"); float
		 * annualInterest=sc.nextFloat(); float monthlyInterest= annualInterest /
		 * percentage/monthsInAYear;
		 * 
		 * System.out.print("Period (Years)"); byte years=sc.nextByte(); int
		 * numberOfPayments= years * monthsInAYear;
		 * 
		 * double emi = principal*(monthlyInterest * Math.pow(1+monthlyInterest,
		 * numberOfPayments))/(Math.pow(1+monthlyInterest , numberOfPayments) -1);
		 * 
		 * System.out.println("EMI Amount for interest values will be " + emi);
		 */
		System.out.print("----------Welcome to AU Small Finance Bank-------------");

		Scanner sc = new Scanner(System.in);

		int percentage = 100;
		int monthsInAYear = 12;

		float monthlyInterest = 0;
		int numberOfPayments = 0;
		int principal = 0;

		while (true) {
			System.out.print("Principal (1000-10000000)");
			principal = sc.nextInt();
			if (principal > 1000 || principal < 10000000) {
				System.err.print("The amount you are trying is not exceptable");
			} else {
				break;
			}
		}
		while (true) {
			System.out.print("Annual Interest Rate (1-30)");
			float annualInterest = sc.nextFloat();
			if (annualInterest < 1 || annualInterest > 30) {
				System.err.print("Not exceptable");
			} else {
				monthlyInterest = annualInterest / percentage / monthsInAYear;
				break;

			}

			System.out.print("Period (Years) (1-20)");
			byte years = sc.nextByte();
			if (years > 1 || years < 20) {
				System.out.print("Not eligible");
			}

			else {
				numberOfPayments = years * monthsInAYear;
				break;
			}
			double emi = principal * (monthlyInterest * Math.pow(1 + monthlyInterest, numberOfPayments))
					/ (Math.pow(1 + monthlyInterest, numberOfPayments) - 1);

			System.out.println("EMI Amount for interest values will be " + emi);
		}
	}

}
